import Ticker from './Ticker'
import ChunkCollection from './chunk/ChunkCollection'
import BaseChunk from './chunk/BaseChunk'
import Chunk from './chunk/Chunk'
import SleepChunk from './chunk/SleepChunk'
import Emitter from 'eventemitter3'
import clone from 'lodash/clone'
class Tinger {
    /**
     * 以动画形式改变对象属性数值
     * @param {Object} data 需要变化的对象，这个值在tinger实例tick阶段，都会改变其属性数值
     * @example 
     * var tinger = new Tinger({x:1,y:2});
     * tinger.go({x:12,y:14});
     * tinger.play();
     */
    constructor(data,config){
        /**
         * @type {Object} 就是传进来的参数对象
         */
        this.data = data;
        /**
         * @type {Ticker} 时钟
         */
        this.ticker = new Ticker();
        /**
         * @type {ChunkCollection} 帧集合
         */
        this.chunks =new ChunkCollection();
        this.emitter = new Emitter();
        /**
         * @type {string} 当前状态 noStart | playing | pause | stop | end
         */
        this.state = 'noStart';
        this.ticker.on('tick',()=>{
            this.chunks.cursor.step();
            if(this.chunks.cursor.chunk instanceof BaseChunk){
                let data = this.chunks.cursor.chunk.getData(this.chunks.cursor.chunkCursor);
                for(let key in data){
                    this.data[key] = data[key];
                }
            }
            this.emitter.emit('tick',this.chunks.cursor);
            if(this.chunks.cursor.chunkCursor === 1){
                this.emitter.emit('tick:key',this.chunks.cursor);
            }
            if(this.chunks.cursor.index >= this.chunks.length){
                this.ticker.stop();
                this.state ='end';
                setTimeout(()=>{
                    this.emitter.emit('end');
                })
            }
        })
    }
    /**
     * 动画改变对象数字属性，支持链式调用
     * @param {Object} data 需要变化到这个终点值
     * @param {Object|number|undefined} config 当config为number时，这个就是动画的执行时间，表示在n毫秒时间里将原来的对象逐渐改变到data的属性值；如果不传默认是1000毫秒
     * @param {number} config.time 动画的执行时间
     * @param {string} config.label 设置一个文本以便识别这个帧
     * @param {duration} config.duration 默认easing 线性动画改变，如果设置为none，则取消动画过程
     * @return {Tinger} 返回自身以支持链式调用
     * 
     * @example 
     * tinger.go({x:10,y:20},2000)//表示2s渐变到这个数值
     * @example
     * tinger.go({x:10,y:20},{time:2000,duration:'none'}) //表示2s后直接到达这个数值，不需要渐变
     */
    go(data,config){
        if(this.state ==='destroy'){
            throw new Error('该tinger实例已被销毁')
            return false;
        }
        config = config || {};
        if(typeof config ==='number'){
            config = {time:config}
        }
        config.time = config.time===undefined?1000:config.time;
        let frame = parseInt(config.time/1000)*60;
        let startData = null;
        if(this.chunks._chunks.length<=0){
            startData = clone(this.data);//第一个数据要克隆，第一个数据是会改变的
        }else{
            startData = this.chunks._chunks[this.chunks._chunks.length-1].data;
        }
        this.chunks.add(new Chunk(startData,data,{length:frame,label:config.label,duration:config.duration}));
        return this;
    }
    /**
     * 函数，支持链式调用
     * @param {number} time 等待的毫秒数
     * @return {Tinger} 返回自身，以支持链式调用
     */
    wait(time){
        if(this.state ==='destroy'){
            throw new Error('该tinger实例已被销毁')
            return false;
        }
        time =time===undefined?1000:time;
        let frame = parseInt(time/1000)*60;
        let startData = null;
        if(this.chunks._chunks.length<=0){
            startData = clone(this.data);
        }else{
            startData = this.chunks._chunks[this.chunks._chunks.length-1].data;
        }
        this.chunks.add(new Chunk(startData,startData,{length:frame}));
        return this;
    }
    /**
     * 该方法会以倍数的方式拉伸影片的长度
     * @param {number} num 缩放倍数
     */
    scale(num){
        if(this.state ==='destroy'){
            throw new Error('该tinger实例已被销毁')
            return false;
        }
        this.chunks.scale(num);
    }
    run(){
        this.play();
    }
    /**
     * 播放
     */
    play(){
        if(this.state ==='destroy'){
            throw new Error('该tinger实例已被销毁')
            return false;
        }
        this.state = 'playing';
        this.ticker.play();
    }
    /**
     * 重新播放
     */
    replay(){
        this.setCursor(0);
        this.play();
    }
    /**
     * 销毁
     */
    destroy(){
        if(this.state ==='destroy'){
            throw new Error('该tinger实例已被销毁')
            return false;
        }
        this.state = 'destroy';
        this.ticker.destroy();
        this.ticker = null;
        this.emitter.removeAllListeners();
        this.emitter = null;
        this.chunks = null;
    }
    /**
     * 暂停
     */
    pause(){
        if(this.state ==='destroy'){
            throw new Error('该tinger实例已被销毁')
            return false;
        }
        this.state = 'pause';
        this.ticker.pause();
    }
    /**
     * 停止
     */
    stop(){
        if(this.state ==='destroy'){
            throw new Error('该tinger实例已被销毁')
            return false;
        }
        this.ticker.stop();
        this.state ='stop';
    }
    /**
     * 
     * @param {number} cursorNum 设置游标位置
     */
    setCursor(cursorNum){
        if(this.state ==='destroy'){
            throw new Error('该tinger实例已被销毁')
            return false;
        }
        this.chunks.cursor.setIndex(cursorNum);
    }
    /**
     * 消息订阅
     * @params {string} name 消息名称 tick(每帧都会触发)|tick:key(进入关键帧触发)|end(播放到最后一帧触发)
     * @params {function} 触发消息时的回调函数
     */
    on(){
        if(this.state ==='destroy'){
            throw new Error('该tinger实例已被销毁')
            return false;
        }
        this.emitter.on.apply(this.emitter,arguments);
    }
}
export default Tinger;
