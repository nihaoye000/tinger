class BaseChunk {
  /**
   * 关键帧基类
   * @param {Object|number} config 配置,当传入数字，即为config.length
   * @param {number} config.length 帧长度，不传默认60(1s的帧数)
   * @param {string} config.label 帧标签
   * @param {string|undefined} config.duration easing|none 帧变化的曲线，不传默认是线性的，当传入none时表示不需要渐变。
   */
  constructor(config) {
    config = config || { length: 60 };
    if (typeof config === "number") {
      config = { length: config };
    }
    /**
     * @type {number} 帧长度
     */
    this.length = config.length;
    /**
     * @type {string} 帧标签
     */
    this.label = config.label;
    /**
     * @type {number} 缩放倍数
     */
    this.scaleNum = config.scale || 1;
    if(this.scaleNum!==1){
      this.scale(this.scaleNum);
    }
  }
  /**
   * 缩放
   * @param {number} num 
   */
  scale(num) {
    if (num < 0) {
      return;
    }
    let oldLength = this.length;
    this.length = parseInt(this.length * num);
    if (this.collection) {
      this.collection.length += this.length - oldLength;
    }
    this.scaleNum = num;
  }
}
export default BaseChunk;
