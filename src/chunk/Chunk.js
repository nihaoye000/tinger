import BaseChunk from './BaseChunk'
class Chunk extends BaseChunk {
  /**
   * 动画关键帧类
   * @param {Object} preData 前置数据
   * @param {Object} data 后置数据
   * @param {Object|number} config 配置,当传入数字，即为config.length
   * @param {number} config.length 帧长度，不传默认60(1s的帧数)
   * @param {string} config.label 帧标签
   * @param {string|undefined} config.duration easing|none 帧变化的曲线，不传默认是线性的，当传入none时表示不需要渐变。
   */
  constructor(preData, data, config) {
    super(config);
    /**
     * @type {Object} 一般就是上一帧的数据
     */
    this.preData = preData;
    /**
     * @type {Object} 当前的数据
     */
    this.data = data;
    config = config ||{};
    /**
     * @type {string} 帧动画的类型  目前只有 none和easing
     */
    this.duration = config.duration||'easing';
  }
  /**
   * 
   * @param {number} length 根据关键帧索引获取某帧属性数据
   * @return {Object}
   */
  getData(length) {
    let obj = {};
    if (length < 0) {
      return false;
    } else if (length > this.length) {
      return false;
    }
    if(length<this.length){
      for (let key in this.data) {
        if(this.duration === 'easing'||!this.duration){
          if (typeof this.data[key] === "number" && typeof this.preData[key] === "number") {
            obj[key] = this._calcEasingNumber(
              this.preData[key],
              this.data[key],
              length
            );
          }else{
            obj[key] = this.preData[key];
          }
        }
      }
    }else{//相等的情况
      for (let key in this.data) {
        obj[key] = this.data[key];
      }
    }
    return obj;
  }
  _calcEasingNumber(num1, num2, length) {
    return num1 + (num2 - num1) / this.length* length;
  }
}
export default Chunk;
