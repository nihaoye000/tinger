import BaseChunk from './BaseChunk'
import clone from 'lodash/clone'
class SleepChunk extends BaseChunk {
    constructor(time,data){
        super(time);
        this.preData = data;
        this.data = data;
    }
    getData(){
        return this.data;
    }
}
export default SleepChunk;