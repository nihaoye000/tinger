
class Cursor{
    /**
     * 游标类
     * @param {ChunkCollection} chunkCollection
     */
    constructor(chunkCollection){
        /**
         * @type {ChunkCollection}
         */
        this.chunkCollection = chunkCollection;
        /**
         * @type {number} 游标当前位置
         */
        this.index = 0;
        /**
         * @type {Chunk} 游标当前所在的关键帧
         */
        this.chunk = null;
        /**
         * @type {number} 游标相对于当前关键帧的位置
         */
        this.chunkCursor = null;
        /**
         * @type {number} 当前游标所在关键帧的帧索引，方便取上一帧或者下一帧
         */
        this.chunkIndex = null;
    }
    /**
     * 设置游标索引位置
     * @param {number} index 索引值
     */
    setIndex(index){
        let chunks = this.chunkCollection._chunks;
        let pickLength = 0;
        let tmpLength = 0;
        let i = 0;
        if(index===0){
            this.chunkCursor =0;
            this.chunk = chunks[0];
            this.chunkIndex = 0;
            this.index = 0;
            return this;
        }
        if(this.chunkCollection.length<=0){
            return false;
        }
        if(index>this.chunkCollection.length){
            return false;
        }
        if(index<0){
            return false;
        }
        for(let item of chunks){
            tmpLength = pickLength + item.length;
            if(tmpLength > length){
                this.chunkCursor = index - pickLength;
                this.chunk = item;
                this.chunkIndex = i;
                this.index = index;
                break;
            }else if(tmpLength === index){
                this.chunkCursor = item.length;
                this.chunk = item;
                this.chunkIndex = i;
                this.index = index;
                break;
            }else{
                pickLength = tmpLength;
            }
            i+=1;
        }
        return this;
    }
    /**
     * 步进游标
     */
    step(){
        if(this.index >= this.chunkCollection.length){
            return false;
        }
        this.index += 1;
        this.chunkCursor +=1;
        if(this.chunkCursor <= this.chunk.length){
            return this;
        }
        this.chunkCursor = 1;//下一帧的开始帧游标,从1开始
        this.chunkIndex += 1;
        this.chunk = this.chunkCollection._chunks[this.chunkIndex];
        return this;
    }
}
export default Cursor;