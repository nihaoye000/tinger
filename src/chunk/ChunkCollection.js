import BaseChunk from './BaseChunk'
import Cursor from './Cursor';
class ChunkCollection{
    /**
     * 帧集合
     */
    constructor(){
        this.length = 0;
        this._chunks = [];
        this.cursor = new Cursor(this);
        this.scaleNum = 1;
    }
    getChunks(){
        return this._chunks;
    }
    add(chunk){
        if(chunk instanceof BaseChunk){
            this._chunks.push(chunk);
            if(this.length<=0){
                this.cursor.setIndex(0);
            }
            this.length += chunk.length;
            chunk.collection = this;
            chunk.scale(this.scaleNum);
            return chunk;
        }
    }
    remove(chunk){
        for(var i=0;i<this._chunks.length;i++){
            if(this._chunks[i]===chunk){
                this._chunks.splice(i,1);
                this.length -= chunk.length;
                break;
            }
        }
        if(this.cursor.index>this.length){
            this.cursor.setIndex(this.length);
        }
    }
    getChunkByLabel(label){
        for(let item of this._chunks){
            if(item.label && item.label === label){
                return item;
            }
        }
    }
    /**
     * 
     * @param {num} index 游标的位置
     * @return {Cursor|boolean} 该方法会返回一个新的cursor实例，而不是chunkColletion中的cursor实例，如果游标位置越界或者小于0，则返回false
     */
    pickCursor(index){
        if(index<0||index>this.length){
            return false;
        }
        let cursor = new Cursor(this);
        cursor.setIndex(index);
        return cursor;
    }
    getCursor(){
        return this.cursor;
    }
    /**
     * 通过关键帧获取游标数值
     * @param {BaseChunk} chunk
     * @return {Cursor} 返回新的游标
     */
    getCursorByChunk(chunk){
        let index = 1;
        for(let item of this._chunks){
            if(chunk === item){
                return cursor;
            }else{
                index += item.length;
            }
        }
        let cursor = new Cursor(this);
        cursor.setIndex(index);
        return cursor
    }
    _calcLength(){
        let length = 0;
        for(let item of this._chunks){
            length += item.length;
        }
        this.length = length;
        return length;
    }
    /**
     * 缩放数据列帧长
     * @param {number} num 缩放倍数
     */
    scale(num){
        if(num<0){
            return;
        }
        for(let item of this._chunks){
            item.scale(num);
        }
        let index = parseInt(this.cursor.index * num);
        this.cursor.setIndex(index);
        this.scaleNum = num;
    }
}
export default ChunkCollection;