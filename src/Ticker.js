import Emitter from 'eventemitter3'

let animationFrame = (function(){
    return  window.requestAnimationFrame       ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame    ||
            window.oRequestAnimationFrame      ||
            window.msRequestAnimationFrame     ||
            function(callback,){
                window.setTimeout(callback, 1000 / 60);
            };
})();
class Ticker{
    /**
     * 滴答计时器，1秒60帧，即1秒滴答60次
     */
    constructor(){
        this.emitter = new Emitter();
        /**
         * @type {string} ticker状态: noStart | stop | playing | pause | destroy 
         */
        this.state ='noStart';
    }
    /**
     * 播放
     */
    play(){
        if(this.state === 'destroy'){
            throw new Error('该ticker实例已被销毁');
            return false;
        }
        if(this.state !=='playing'){
            this.state = 'playing';
            this._runTick();
        }
    }
    _runTick(){
        animationFrame(()=>{
            this.emitter.emit('tick');
            if(this.state === 'playing'){
                this._runTick();
            }
        })
    }
    /**
     * 停止
     */
    stop(){
        if(this.state === 'destroy'){
            throw new Error('该ticker实例已被销毁');
            return false;
        }
        this.state = 'stop';
    }
    /**
     * 暂停
     */
    pause(){
        if(this.state === 'destroy'){
            throw new Error('该ticker实例已被销毁');
            return false;
        }
        this.state = 'pause';
    }
    /**
     * 销毁
     */
    destroy(){
        if(this.state === 'destroy'){
            throw new Error('该ticker实例已被销毁');
            return false;
        }
        this.state ='destroy';
        this.emitter.removeAllListeners();
        this.emitter = null;
    }
    /**
     * 消息订阅
     * @params {string} name 消息名称 tick(每帧都会触发,1秒触发60次)
     * @params {function} 触发消息时的回调函数
     * @example
     * var ticker = new Ticker();
     * ticker.on('tick',()=>{
     * //do something...
     * })
     *
     */
    on(){
        if(this.state === 'destroy'){
            throw new Error('该ticker实例已被销毁');
            return false;
        }
        this.emitter.on.apply(this.emitter,arguments);
    }
}
export default Ticker;