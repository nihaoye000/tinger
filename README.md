# tinger

#### 介绍
补间帧动画插件

#### 软件架构


#### 安装教程
```html
<script src='./dist/tinger.min.js'></script>
```
```javascript
npm install tinger
```

#### 使用说明

[API Reference](http://nihaoye000.gitee.io/tinger/)

简单的使用示例
```javascript
var tinger = new Tinger({x:2,y:3});//初始化一个tinger实例
tinger.go({x:8,y:10},2000) //定义两秒的执行动画
.wait(3000) //等待3s
.go({x:3,y:2},{time:2000,duration:'none'}); //2s后数值发生变化，不需要动画

tinger.on('tick',()=>{//订阅每一帧触发事件
    //do something...
})
tinger.on('tick:key',()=>{//订阅每一关键帧触发事件
    //do something...
})
tinger.on('end',()=>{//订阅动画执行完成触发事件
    //do something...
})

//动画播放开始
tinger.play();

//动画暂停
//tinger.pause();

//动画重新播放
//tinger.replay();

//缩放动画时间轴
//tinger.scale(0.5) //动画加快了一倍

//销毁
//tinger.destroy()
```
